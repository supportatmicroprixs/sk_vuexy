<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Validator;

class APIVueController extends Controller
{
    public function memberList()
    {
        try {
            $user_id = 1;

            $error = "";
            $json = array();

            if($user_id == ""){
                $error = "Please enter valid User";
                $json = array('status_code' => '0', 'message' => $error);
            }

            if($error == ""){
                $userData = array();
                
                $date   = date('Y-m-d H:i:s');
                $user = User::find($user_id);
                if($user) 
                {
                    $userExists = User::where('users.created_by',$user_id)
                            ->count();

                    if($userExists > 0)
                    {
                        $userDistributor = User::where('users.created_by',$user_id)
                            ->get(['users.*']);

                        $userData = array();

                        if($userDistributor)
                        {
                            foreach($userDistributor as $row)
                            {
                                $status = "";

                                if($row->status == 1)
                                {
                                    $status = "pending";
                                }
                                else if($row->status == 0)
                                {
                                    $status = "inactive";
                                }
                                else if($row->status == 2)
                                {
                                    $status = "active";
                                }

                                $role = $row->role;
                                
                                if($row->avatar == "")
                                {
                                    $row->avatar = "images/no-image.png";
                                }

                                $userData[] = array('id' => "".$row->id, 'fullName' => $row->name, 'username' => $row->username, 'email' => $row->email, 'username' => $row->username, 'role' => $role, 'status' => $status, 'ewallet' => "₹ " .number_format($row->ewallet, 2), 'aeps_wallet' => "₹ " .number_format($row->aeps_wallet, 2), 'company' => '', 'currentPlan' => 'enterprise', 'avatar' => url('/')."/".$row->avatar);
                            }
                        }

                        $status_code = '1';
                        $message = 'Member list';
                        $json = array('status_code' => $status_code, 'message' => $message, 'users' => $userData);
                    }
                    else
                    {
                        $status_code = '0';
                        $message = 'No member exists.';
                
                        $json = array('status_code' => $status_code, 'message' => $message);
                    }
                }
                else
                {
                    $status_code = '0';
                    $message = 'User not exists';
            
                    $json = array('status_code' => $status_code, 'message' => $message);
                }
            }   
        }
        catch(\Exception $e) {
            $status_code = '0';
            $message = $e->getMessage();//$e->getTraceAsString(); getMessage //
    
            $json = array('status_code' => $status_code, 'message' => $message);
        }
    
        return response()->json($json, 200);
    }

    public function member($id)
    {
        try {
            $user_id = 1;

            $error = "";
            $json = array();

            if($user_id == ""){
                $error = "Please enter valid User";
                $json = array('status_code' => '0', 'message' => $error);
            }

            if($error == ""){
                $userData = array();
                
                $date   = date('Y-m-d H:i:s');
                $user = User::find($user_id);
                if($user) 
                {
                    $userExists = User::where('id', $id)
                            ->count();

                    if($userExists > 0)
                    {
                        $row = User::where('id', $id)
                            ->first();

                        if($row)
                        {
                            $status = "";

                            if($row->status == 1)
                            {
                                $status = "pending";
                            }
                            else if($row->status == 0)
                            {
                                $status = "inactive";
                            }
                            else if($row->status == 2)
                            {
                                $status = "active";
                            }

                            $role = $row->role;
                            
                            if($row->avatar == "")
                            {
                                $row->avatar = "images/no-image.png";
                            }

                            $status_code = '1';
                            $message = 'Member list';
                            $json = array('status_code' => $status_code, 'message' => $message, 'id' => "".$row->id, 'fullName' => $row->name, 'username' => $row->username, 'email' => $row->email, 'country' => $row->country, 'contact' => $row->mobile, 'username' => $row->username, 'role' => $role, 'status' => $status, 'ewallet' => number_format($row->ewallet, 2), 'aeps_wallet' => "".number_format($row->aeps_wallet, 2), 'company' => '', 'currentPlan' => 'enterprise', 'avatar' => url('/')."/".$row->avatar);
                        }
                    }
                    else
                    {
                        $status_code = '0';
                        $message = 'No member exists.';
                
                        $json = array('status_code' => $status_code, 'message' => $message);
                    }
                }
                else
                {
                    $status_code = '0';
                    $message = 'User not exists';
            
                    $json = array('status_code' => $status_code, 'message' => $message);
                }
            }   
        }
        catch(\Exception $e) {
            $status_code = '0';
            $message = $e->getMessage();//$e->getTraceAsString(); getMessage //
    
            $json = array('status_code' => $status_code, 'message' => $message);
        }
    
        return response()->json($json, 200);
    }
}