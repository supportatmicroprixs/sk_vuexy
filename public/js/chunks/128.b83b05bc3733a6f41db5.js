(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[128],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/miscellaneous/NotAuthorized.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/miscellaneous/NotAuthorized.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var bootstrap_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! bootstrap-vue */ "./node_modules/bootstrap-vue/esm/index.js");
/* harmony import */ var _core_layouts_components_Logo_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @core/layouts/components/Logo.vue */ "./resources/js/src/@core/layouts/components/Logo.vue");
/* harmony import */ var _store_index__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/store/index */ "./resources/js/src/store/index.js");
/* harmony import */ var _auth_utils__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/auth/utils */ "./resources/js/src/auth/utils.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* eslint-disable global-require */




/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    BLink: bootstrap_vue__WEBPACK_IMPORTED_MODULE_0__["BLink"],
    BImg: bootstrap_vue__WEBPACK_IMPORTED_MODULE_0__["BImg"],
    BButton: bootstrap_vue__WEBPACK_IMPORTED_MODULE_0__["BButton"],
    VuexyLogo: _core_layouts_components_Logo_vue__WEBPACK_IMPORTED_MODULE_1__["default"]
  },
  data: function data() {
    return {
      downImg: __webpack_require__(/*! @/assets/images/pages/not-authorized.svg */ "./resources/js/src/assets/images/pages/not-authorized.svg")
    };
  },
  computed: {
    imgUrl: function imgUrl() {
      if (_store_index__WEBPACK_IMPORTED_MODULE_2__["default"].state.appConfig.layout.skin === 'dark') {
        // eslint-disable-next-line vue/no-side-effects-in-computed-properties
        this.downImg = __webpack_require__(/*! @/assets/images/pages/not-authorized-dark.svg */ "./resources/js/src/assets/images/pages/not-authorized-dark.svg");
        return this.downImg;
      }

      return this.downImg;
    }
  },
  methods: {
    loginRoute: function loginRoute() {
      var user = JSON.parse(localStorage.getItem('userData'));
      return Object(_auth_utils__WEBPACK_IMPORTED_MODULE_3__["getHomeRouteForLoggedInUser"])(user ? user.role : null);
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/miscellaneous/NotAuthorized.vue?vue&type=style&index=0&lang=scss&":
/*!********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/sass-loader/dist/cjs.js??ref--11-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/miscellaneous/NotAuthorized.vue?vue&type=style&index=0&lang=scss& ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, ".misc-wrapper {\n  display: flex;\n  flex-basis: 100%;\n  min-height: 100vh;\n  width: 100%;\n  align-items: center;\n  justify-content: center;\n}\n.misc-wrapper .misc-inner {\n  position: relative;\n  max-width: 750px;\n}\n.misc-wrapper .brand-logo {\n  display: flex;\n  justify-content: center;\n  position: absolute;\n  top: 2rem;\n}\n[dir] .misc-wrapper .brand-logo {\n  margin: 0;\n}\n[dir=ltr] .misc-wrapper .brand-logo {\n  left: 2rem;\n}\n[dir=rtl] .misc-wrapper .brand-logo {\n  right: 2rem;\n}\n.misc-wrapper .brand-logo .brand-text {\n  font-weight: 600;\n}\n@media (max-height: 625px) {\n[dir] .misc-wrapper .misc-inner {\n    margin-top: 4rem;\n}\n}\n.brand-logo svg {\n  height: 28px;\n  width: 40.95px;\n}", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/dist/cjs.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/miscellaneous/NotAuthorized.vue?vue&type=style&index=0&lang=scss&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader/dist/cjs.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/sass-loader/dist/cjs.js??ref--11-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/miscellaneous/NotAuthorized.vue?vue&type=style&index=0&lang=scss& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../../node_modules/css-loader/dist/cjs.js!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../../../node_modules/sass-loader/dist/cjs.js??ref--11-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./NotAuthorized.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/dist/cjs.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/miscellaneous/NotAuthorized.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/@core/layouts/components/Logo.vue?vue&type=template&id=46f77075&":
/*!*************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/@core/layouts/components/Logo.vue?vue&type=template&id=46f77075& ***!
  \*************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "svg",
    {
      attrs: {
        version: "1.0",
        xmlns: "http://www.w3.org/2000/svg",
        width: "131.000000pt",
        height: "159.000000pt",
        viewBox: "0 0 131.000000 159.000000",
        preserveAspectRatio: "xMidYMid meet",
      },
    },
    [
      _c(
        "g",
        {
          attrs: {
            transform:
              "translate(0.000000,159.000000) scale(0.100000,-0.100000)",
            fill: "#000000",
            stroke: "none",
          },
        },
        [
          _c("path", {
            attrs: {
              d: "M580 1020 l0 -510 100 0 100 0 1 53 c1 48 4 55 41 87 23 19 43 31 45\n25 2 -5 25 -46 51 -90 l48 -80 103 -3 c84 -2 102 0 98 11 -3 7 -42 71 -86 141\n-45 70 -82 131 -84 135 -1 5 38 42 87 82 l89 74 -119 3 -118 3 -78 -78 -78\n-78 0 368 0 367 -100 0 -100 0 0 -510z",
            },
          }),
          _vm._v(" "),
          _c("path", {
            attrs: {
              d: "M287 946 c-63 -17 -99 -45 -115 -88 -31 -82 34 -138 198 -173 85 -18\n120 -37 120 -65 0 -41 -74 -63 -134 -40 -14 5 -32 23 -41 40 l-16 31 -77 -7\nc-42 -4 -78 -8 -80 -9 -2 -2 3 -18 11 -37 18 -43 67 -84 115 -98 48 -13 206\n-13 255 1 l37 10 0 124 0 123 -32 11 c-18 5 -62 15 -98 22 -99 18 -139 53 -91\n79 42 22 115 4 126 -32 5 -14 16 -18 51 -18 l44 0 0 55 c0 51 -2 56 -27 66\n-45 18 -192 21 -246 5z",
            },
          }),
          _vm._v(" "),
          _c("path", {
            attrs: {
              d: "M140 375 c0 -68 2 -75 20 -75 16 0 20 7 20 30 0 27 3 30 30 30 20 0\n30 5 30 15 0 10 -10 15 -30 15 -20 0 -30 5 -30 15 0 10 11 15 35 15 24 0 35 5\n35 15 0 12 -13 15 -55 15 l-55 0 0 -75z",
            },
          }),
          _vm._v(" "),
          _c("path", {
            attrs: {
              d: "M280 375 c0 -73 1 -75 25 -75 24 0 25 2 25 75 0 73 -1 75 -25 75 -24\n0 -25 -2 -25 -75z",
            },
          }),
          _vm._v(" "),
          _c("path", {
            attrs: {
              d: "M370 375 c0 -68 2 -75 20 -75 17 0 20 7 20 43 l0 42 28 -42 c18 -30\n34 -43 50 -43 21 0 22 4 22 75 0 68 -2 75 -19 75 -16 0 -20 -8 -23 -42 l-3\n-42 -28 42 c-17 26 -35 42 -47 42 -18 0 -20 -7 -20 -75z",
            },
          }),
          _vm._v(" "),
          _c("path", {
            attrs: {
              d: "M556 380 c-14 -39 -26 -73 -26 -76 0 -11 39 -2 44 11 7 20 53 19 64\n-1 5 -9 18 -14 29 -12 20 3 20 4 -6 73 -23 59 -31 70 -53 73 -23 3 -28 -3 -52\n-68z m64 -7 c0 -7 -7 -13 -16 -13 -11 0 -14 5 -10 16 3 9 6 21 6 27 0 16 20\n-14 20 -30z",
            },
          }),
          _vm._v(" "),
          _c("path", {
            attrs: {
              d: "M710 375 c0 -68 2 -75 19 -75 16 0 20 8 23 42 l3 42 28 -42 c17 -26\n35 -42 47 -42 18 0 20 7 20 75 0 68 -2 75 -19 75 -16 0 -20 -8 -23 -42 l-3\n-42 -28 42 c-17 26 -35 42 -47 42 -18 0 -20 -7 -20 -75z",
            },
          }),
          _vm._v(" "),
          _c("path", {
            attrs: {
              d: "M900 430 c-30 -30 -22 -98 14 -122 31 -21 106 2 106 32 0 15 -29 19\n-40 4 -30 -35 -71 8 -51 52 10 23 51 34 51 14 0 -12 10 -13 29 -1 12 8 12 12\n2 25 -20 23 -86 21 -111 -4z",
            },
          }),
          _vm._v(" "),
          _c("path", {
            attrs: {
              d: "M1050 375 l0 -75 65 0 c51 0 65 3 65 15 0 11 -11 15 -40 15 -33 0\n-40 3 -40 20 0 16 7 20 35 20 19 0 35 5 35 10 0 6 -16 10 -35 10 -24 0 -35 5\n-35 15 0 10 11 15 35 15 24 0 35 5 35 15 0 12 -13 15 -60 15 l-60 0 0 -75z",
            },
          }),
          _vm._v(" "),
          _c("path", {
            attrs: {
              d: "M135 210 c-3 -6 1 -7 9 -4 18 7 21 14 7 14 -6 0 -13 -4 -16 -10z",
            },
          }),
          _vm._v(" "),
          _c("path", {
            attrs: {
              d: "M420 211 c0 -5 5 -13 10 -16 6 -3 10 -2 10 4 0 5 -4 13 -10 16 -5 3\n-10 2 -10 -4z",
            },
          }),
          _vm._v(" "),
          _c("path", {
            attrs: {
              d: "M571 194 c-1 -19 4 -31 12 -31 7 0 13 8 13 18 0 10 -3 16 -7 13 -4\n-2 -10 4 -12 13 -3 10 -5 4 -6 -13z",
            },
          }),
          _vm._v(" "),
          _c("path", {
            attrs: {
              d: "M830 206 c0 -8 -8 -17 -17 -19 -16 -4 -16 -5 -1 -6 9 0 19 -9 21 -18\n3 -10 5 -1 6 20 0 20 -1 37 -4 37 -3 0 -5 -6 -5 -14z",
            },
          }),
          _vm._v(" "),
          _c("path", {
            attrs: {
              d: "M492 180 c0 -14 2 -19 5 -12 2 6 2 18 0 25 -3 6 -5 1 -5 -13z",
            },
          }),
          _vm._v(" "),
          _c("path", {
            attrs: {
              d: "M623 181 c1 -14 9 -26 17 -28 10 -3 11 -2 4 4 -7 4 -14 17 -17 28 -5\n16 -5 15 -4 -4z",
            },
          }),
          _vm._v(" "),
          _c("path", {
            attrs: {
              d: "M881 183 c0 -13 -6 -23 -12 -23 -5 0 -7 5 -3 12 5 7 3 8 -5 3 -9 -5\n-10 -10 -2 -18 15 -15 34 5 28 29 -4 16 -5 15 -6 -3z",
            },
          }),
          _vm._v(" "),
          _c("path", {
            attrs: {
              d: "M912 180 c0 -14 2 -19 5 -12 2 6 2 18 0 25 -3 6 -5 1 -5 -13z",
            },
          }),
          _vm._v(" "),
          _c("path", {
            attrs: {
              d: "M930 180 c0 -11 2 -20 4 -20 2 0 6 9 9 20 3 11 1 20 -4 20 -5 0 -9\n-9 -9 -20z",
            },
          }),
          _vm._v(" "),
          _c("path", {
            attrs: {
              d: "M962 175 c0 -16 2 -22 5 -12 2 9 2 23 0 30 -3 6 -5 -1 -5 -18z",
            },
          }),
          _vm._v(" "),
          _c("path", {
            attrs: {
              d: "M155 178 c4 -11 0 -18 -12 -21 -15 -4 -16 -5 -1 -6 22 -1 33 18 19\n32 -9 9 -11 8 -6 -5z",
            },
          }),
          _vm._v(" "),
          _c("path", {
            attrs: {
              d: "M211 174 c0 -11 3 -14 6 -6 3 7 2 16 -1 19 -3 4 -6 -2 -5 -13z",
            },
          }),
          _vm._v(" "),
          _c("path", {
            attrs: {
              d: "M230 171 c0 -6 4 -13 10 -16 6 -3 7 1 4 9 -7 18 -14 21 -14 7z",
            },
          }),
          _vm._v(" "),
          _c("path", {
            attrs: {
              d: "M1064 172 c3 -6 -2 -13 -11 -15 -16 -4 -16 -5 -1 -6 21 -1 32 15 17\n25 -7 4 -9 3 -5 -4z",
            },
          }),
          _vm._v(" "),
          _c("path", {
            attrs: {
              d: "M1135 171 c-7 -12 12 -24 25 -16 6 3 10 12 9 18 0 9 -2 9 -6 0 -2 -7\n-9 -13 -14 -13 -6 0 -7 5 -4 10 3 6 4 10 1 10 -3 0 -7 -4 -11 -9z",
            },
          }),
          _vm._v(" "),
          _c("path", {
            attrs: {
              d: "M530 160 c-9 -6 -10 -10 -3 -10 6 0 15 5 18 10 8 12 4 12 -15 0z",
            },
          }),
          _vm._v(" "),
          _c("path", {
            attrs: {
              d: "M963 109 c-18 -5 -33 -16 -33 -24 0 -12 6 -12 37 2 25 10 57 14 96\n11 36 -3 52 -1 43 4 -25 14 -108 18 -143 7z",
            },
          }),
        ]
      ),
    ]
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/miscellaneous/NotAuthorized.vue?vue&type=template&id=158135e8&":
/*!***********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/miscellaneous/NotAuthorized.vue?vue&type=template&id=158135e8& ***!
  \***********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "misc-wrapper" },
    [
      _c(
        "b-link",
        { staticClass: "brand-logo" },
        [
          _c("vuexy-logo"),
          _vm._v(" "),
          _c("h2", { staticClass: "brand-text text-primary ml-1" }, [
            _vm._v("\n      Vuexy\n    "),
          ]),
        ],
        1
      ),
      _vm._v(" "),
      _c("div", { staticClass: "misc-inner p-2 p-sm-3" }, [
        _c(
          "div",
          { staticClass: "w-100 text-center" },
          [
            _c("h2", { staticClass: "mb-1" }, [
              _vm._v("\n        You are not authorized! 🔐\n      "),
            ]),
            _vm._v(" "),
            _c("p", { staticClass: "mb-2" }, [
              _vm._v(
                "\n        You don’t have permission to access this page. Go Home!!\n      "
              ),
            ]),
            _vm._v(" "),
            _c(
              "b-button",
              {
                staticClass: "mb-1 btn-sm-block",
                attrs: { variant: "primary", to: _vm.loginRoute() },
              },
              [_vm._v("Back to Home")]
            ),
            _vm._v(" "),
            _c("b-img", {
              attrs: { fluid: "", src: _vm.imgUrl, alt: "Not authorized page" },
            }),
          ],
          1
        ),
      ]),
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/@core/layouts/components/Logo.vue":
/*!************************************************************!*\
  !*** ./resources/js/src/@core/layouts/components/Logo.vue ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Logo_vue_vue_type_template_id_46f77075___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Logo.vue?vue&type=template&id=46f77075& */ "./resources/js/src/@core/layouts/components/Logo.vue?vue&type=template&id=46f77075&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  script,
  _Logo_vue_vue_type_template_id_46f77075___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Logo_vue_vue_type_template_id_46f77075___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/@core/layouts/components/Logo.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/@core/layouts/components/Logo.vue?vue&type=template&id=46f77075&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/src/@core/layouts/components/Logo.vue?vue&type=template&id=46f77075& ***!
  \*******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Logo_vue_vue_type_template_id_46f77075___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Logo.vue?vue&type=template&id=46f77075& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/@core/layouts/components/Logo.vue?vue&type=template&id=46f77075&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Logo_vue_vue_type_template_id_46f77075___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Logo_vue_vue_type_template_id_46f77075___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/src/assets/images/pages/not-authorized-dark.svg":
/*!**********************************************************************!*\
  !*** ./resources/js/src/assets/images/pages/not-authorized-dark.svg ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/_/_/_/_/esskay-vuexy/resources/js/src/assets/images/pages/not-authorized-dark.svg";

/***/ }),

/***/ "./resources/js/src/assets/images/pages/not-authorized.svg":
/*!*****************************************************************!*\
  !*** ./resources/js/src/assets/images/pages/not-authorized.svg ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/_/_/_/_/esskay-vuexy/resources/js/src/assets/images/pages/not-authorized.svg";

/***/ }),

/***/ "./resources/js/src/views/pages/miscellaneous/NotAuthorized.vue":
/*!**********************************************************************!*\
  !*** ./resources/js/src/views/pages/miscellaneous/NotAuthorized.vue ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _NotAuthorized_vue_vue_type_template_id_158135e8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./NotAuthorized.vue?vue&type=template&id=158135e8& */ "./resources/js/src/views/pages/miscellaneous/NotAuthorized.vue?vue&type=template&id=158135e8&");
/* harmony import */ var _NotAuthorized_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./NotAuthorized.vue?vue&type=script&lang=js& */ "./resources/js/src/views/pages/miscellaneous/NotAuthorized.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _NotAuthorized_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./NotAuthorized.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/src/views/pages/miscellaneous/NotAuthorized.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _NotAuthorized_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _NotAuthorized_vue_vue_type_template_id_158135e8___WEBPACK_IMPORTED_MODULE_0__["render"],
  _NotAuthorized_vue_vue_type_template_id_158135e8___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/pages/miscellaneous/NotAuthorized.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/pages/miscellaneous/NotAuthorized.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************!*\
  !*** ./resources/js/src/views/pages/miscellaneous/NotAuthorized.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_NotAuthorized_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./NotAuthorized.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/miscellaneous/NotAuthorized.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_NotAuthorized_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/pages/miscellaneous/NotAuthorized.vue?vue&type=style&index=0&lang=scss&":
/*!********************************************************************************************************!*\
  !*** ./resources/js/src/views/pages/miscellaneous/NotAuthorized.vue?vue&type=style&index=0&lang=scss& ***!
  \********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_dist_cjs_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_sass_loader_dist_cjs_js_ref_11_0_node_modules_vue_loader_lib_index_js_vue_loader_options_NotAuthorized_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/style-loader!../../../../../../node_modules/css-loader/dist/cjs.js!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../../../node_modules/sass-loader/dist/cjs.js??ref--11-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./NotAuthorized.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/dist/cjs.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/miscellaneous/NotAuthorized.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_dist_cjs_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_sass_loader_dist_cjs_js_ref_11_0_node_modules_vue_loader_lib_index_js_vue_loader_options_NotAuthorized_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_dist_cjs_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_sass_loader_dist_cjs_js_ref_11_0_node_modules_vue_loader_lib_index_js_vue_loader_options_NotAuthorized_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_dist_cjs_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_sass_loader_dist_cjs_js_ref_11_0_node_modules_vue_loader_lib_index_js_vue_loader_options_NotAuthorized_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_dist_cjs_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_sass_loader_dist_cjs_js_ref_11_0_node_modules_vue_loader_lib_index_js_vue_loader_options_NotAuthorized_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/src/views/pages/miscellaneous/NotAuthorized.vue?vue&type=template&id=158135e8&":
/*!*****************************************************************************************************!*\
  !*** ./resources/js/src/views/pages/miscellaneous/NotAuthorized.vue?vue&type=template&id=158135e8& ***!
  \*****************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_NotAuthorized_vue_vue_type_template_id_158135e8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./NotAuthorized.vue?vue&type=template&id=158135e8& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/miscellaneous/NotAuthorized.vue?vue&type=template&id=158135e8&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_NotAuthorized_vue_vue_type_template_id_158135e8___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_NotAuthorized_vue_vue_type_template_id_158135e8___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);