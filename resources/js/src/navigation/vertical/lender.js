export default [
  {
    title: 'Lender',
    icon: 'ListIcon',
    children: [
      {
        title: 'Lenders',
        route: 'lender-list',
      },
      {
        title: 'Lender Banking',
        route: 'lender-banking',
      },
      {
        title: 'Lender Banking Detail',
        route: 'lender-banking-detail',
      },
      {
        title: 'Lender Documents',
        route: 'lender-document',
      },
      {
        title: 'Accepted Lender Documents',
        route: 'accepted-lender-documents',
      },
      {
        title: 'Rejected Lender Documents',
        route: 'rejected-lender-documents',
      },
    ]
  },
]