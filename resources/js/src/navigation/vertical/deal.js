export default [
  {
    title: 'Deal',
    icon: 'ListIcon',
    children: [
      {
        title: 'Current Deal Category',
        route: 'currendeal-category-deal',
      },
      {
        title: 'Current Deal',
        route: 'currendeal-deal',
      },
    ]
  },
]