export default [
  {
    title: 'Authentication',
    icon: 'UserIcon',
    children: [
      {
        title: 'User',
        route: 'apps-users-list',
      },
      {
        title: 'Role',
        route: 'apps-role-list',
      },
      {
        title: 'Permission',
        route: 'apps-permission-list',
      },
    ],
  },
]