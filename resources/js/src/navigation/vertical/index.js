/*

Array of object

Top level object can be:
1. Header
2. Group (Group can have navItems as children)
3. navItem

* Supported Options

/--- Header ---/

header

/--- nav Grp ---/

title
icon (if it's on top level)
tag
tagVariant
children

/--- nav Item ---/

icon (if it's on top level)
title
route: [route_obj/route_name] (I have to resolve name somehow from the route obj)
tag
tagVariant

*/
import dashboard from './dashboard'
import master from './master'
import emailSms from './email-sms'
import insight from './insight'
import deal from './deal'
import sanctionLetter from './sanction-letter'
import investment from './investment'
import lender from './lender'
import sanctionUser from './sanction-user'
import internalUser from './internal-user'
import trustee from './trustee'
import additional from './additional'
import user from './user'
/*import appsAndPages from './apps-and-pages'
import others from './others'
import chartsAndMaps from './charts-and-maps'
import uiElements from './ui-elements'
import formAndTable from './forms-and-table'*/

// Array of sections
//export default [...dashboard, ...appsAndPages, ...uiElements, ...formAndTable, ...chartsAndMaps, ...others]
export default [...dashboard, ...master, ...emailSms, ...insight, ...deal, ...sanctionLetter, ...investment, ...lender, ...sanctionUser, ...internalUser, ...trustee, ...additional, ...user]
