export default [
  {
    title: 'Master',
    icon: 'ListIcon',
    children: [
      {
        title: 'Lender Type',
        route: 'lender-type-list',
      },
      {
        title: 'Relevant Party Type',
        route: 'relevant-party-type-list',
      },
      {
        title: 'Trustee Type',
        route: 'trustee-type-list',
      },
      {
        title: 'Instrument Type',
        route: 'instrument-type-list',
      },
      {
        title: 'Facility Type',
        route: 'facility-type-list',
      },
      {
        title: 'Asset Class',
        route: 'asset-class-list',
      },
      {
        title: 'Doc Category',
        route: 'doc-category-list',
      },
      {
        title: 'Banking Arrangment',
        route: 'banking-arrangement-list',
      },
      {
        title: 'Insight Category',
        route: 'insight-category-list',
      },
      {
        title: 'Trans Document Type',
        route: 'trans-document-type-list',
      },
      {
        title: 'Trans Category',
        route: 'trans-category-list',
      },
      {
        title: 'State',
        route: 'state-list',
      },
      {
        title: 'Map State',
        route: 'map-state-list',
      },
      {
        title: 'Company Rating',
        route: 'company-rating-list',
      },
      {
        title: 'District',
        route: 'district-list',
      },
      {
        title: 'Committee',
        route: 'committee-list',
      },
      {
        title: 'Organization Structure',
        route: 'organization-structure-list',
      },
      {
        title: 'Hierarchy Structure',
        route: 'hierarchy-structure-list',
      },
      {
        title: 'Lender Group',
        route: 'lender-group-list',
      },
      {
        title: 'Shareholding Group',
        route: 'shareholding-group-list',
      },
      {
        title: 'Shareholding',
        route: 'shareholding-list',
      },
      {
        title: 'Trustee Group',
        route: 'trustee-group-list',
      },
      {
        title: 'Sanction User Group',
        route: 'sanctionuser-group-list',
      },
    ],
  },
]