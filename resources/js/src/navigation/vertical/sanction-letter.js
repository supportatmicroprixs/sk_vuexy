export default [
  {
    title: 'Sanction Letter',
    icon: 'ListIcon',
    children: [
      {
        title: 'Sanction Letters',
        route: 'sanctionletter-list',
      },
      {
        title: 'Sanction Letter History',
        route: 'sanctionletter-history',
      },
      {
        title: 'Accepted Sanction Letters',
        route: 'accepted-sanctionletter-list',
      },
      {
        title: 'Rejected Sanction Letters',
        route: 'rejected-sanctionletter-list',
      },
    ]    
  },
]