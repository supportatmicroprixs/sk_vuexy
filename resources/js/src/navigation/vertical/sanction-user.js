export default [
  {
    title: 'Sanction Users',
    icon: 'ListIcon',
    children: [
      {
        title: 'Sanction User',
        route: 'sanction-user-list',
      },
    ]
  },
]