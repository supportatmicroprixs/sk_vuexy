export default [
  {
    title: 'Documents Download',
    route: '#',
    icon: 'ListIcon',
  },
  {
    title: 'Email Template',
    route: '#',
    icon: 'ListIcon',
  },
  {
    title: 'SMS Template',
    route: '#',
    icon: 'ListIcon',
  },
  {
    title: 'Pages',
    route: '#',
    icon: 'ListIcon',
  },
  {
    title: 'Settings',
    route: '#',
    icon: 'ListIcon',
  },
  {
    title: 'Contact Us',
    route: '#',
    icon: 'ListIcon',
  },
  {
    title: 'Enquiry',
    route: '#',
    icon: 'ListIcon',
  },
  {
    title: 'Analytics Graph',
    route: '#',
    icon: 'ListIcon',
  },
  {
    title: 'User Login',
    route: '#',
    icon: 'ListIcon',
  },
  {
    title: 'User Login Attempt',
    route: '#',
    icon: 'ListIcon',
  },
]