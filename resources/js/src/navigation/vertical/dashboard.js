export default [
  {
    title: 'Dashboard',
    route: 'dashboard-analytics',
    icon: 'HomeIcon',
    tag: '',
    tagVariant: 'light-warning',
  },
]