export default [
  {
    title: 'Trustee',
    icon: 'ListIcon',
    children: [
      {
        title: 'Trustee',
        route: 'trustee-list',
      },
      {
        title: 'Transaction',
        route: 'transaction-list',
      },
      {
        title: 'Accepted Transaction',
        route: 'accepted-transaction-list',
      },
      {
        title: 'Rejected Transaction',
        route: 'rejected-transaction-list',
      },
      {
        title: 'Trans. Document',
        route: 'transaction-document-list',
      },
      {
        title: 'Accepted Transaction Document',
        route: 'accepted-transaction-document-list',
      },
      {
        title: 'Relevant Party',
        route: 'relevant-party-list',
      },
      {
        title: 'Pool Dynamics',
        route: 'pool-dynamics-list',
      },
    ]
  },
]