export default [
  {
    title: 'Internal Users',
    icon: 'ListIcon',
    children: [
      {
        title: 'Internal User',
        route: 'internal-user-list',
      },
    ]
  },
]