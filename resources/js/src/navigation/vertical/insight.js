export default [
  {
    title: 'Insight',
    icon: 'ListIcon',
    children: [
      {
        title: 'Snapshot',
        route: 'snapshot-insight',
      },
      {
        title: 'Geographical',
        route: 'geographical-insight',
      },
      {
        title: 'Product',
        route: 'product-insight',
      },
      {
        title: 'Asset Quality',
        route: 'assetquality-insight',
      },
      {
        title: 'Portfolio Analysis',
        route: 'portfolio-insight',
      },
      {
        title: 'Capital',
        route: 'networth-insight',
      },
      {
        title: 'Capital Infusions',
        route: 'networthinfusion-insight',
      },
      {
        title: 'Liquidity',
        route: 'liquidity-insight',
      },
      {
        title: 'Resource Mix',
        route: 'resourcemix-insight',
      },
      {
        title: 'Resource Mix Table',
        route: 'resourcemix-table-insight',
      },
      {
        title: 'Resource Mix Ratios',
        route: 'resourcemix-ratios-insight',
      },
      {
        title: 'Resource Mix Driving',
        route: 'resourcemix-driving-insight',
      },
      {
        title: 'Attributes WellTables',
        route: 'attributes-welltable-insight',
      },
      {
        title: 'Resource Mix Overall',
        route: 'resourcemix-overall-insight',
      },
      {
        title: 'Resource Mix Categories',
        route: 'resourcemix-category-insight',
      },
      {
        title: 'Resource Mix Sliders',
        route: 'resourcemix-slider-insight',
      },
      {
        title: 'Covid Lenders',
        route: 'covidlender-insight',
      },
      {
        title: 'Covid Borrowers',
        route: 'covidborrower-insight',
      },
      {
        title: 'Insight Location',
        route: 'insightlocation-insight',
      },
      {
        title: 'Asset Quality1',
        route: 'assetquality1-insight',
      },
      {
        title: 'Asset Quality2',
        route: 'assetquality2-insight',
      },
      {
        title: 'Asset Quality3',
        route: 'assetquality3-insight',
      },
      {
        title: 'Asset Quality4',
        route: 'assetquality4-insight',
      },
      {
        title: 'Composition',
        route: 'composition-insight',
      },
      {
        title: 'Capital',
        route: 'capital-insight',
      },
      {
        title: 'Asset GNPA',
        route: 'assetgnpa-insight',
      },
    ]
  },
]