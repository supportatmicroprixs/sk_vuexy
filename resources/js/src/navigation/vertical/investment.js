export default [
  {
    title: 'Investment',
    icon: 'ListIcon',
    children: [
      {
        title: 'Buy',
        route: 'buy-list',
      },
      {
        title: 'Buy History',
        route: 'buy-history',
      },
      {
        title: 'Accepted Investment Buy',
        route: 'accepted-investmentbuy-list',
      },
      {
        title: 'Rejected Investment Buy',
        route: 'rejected-investmentbuy-list',
      },
      {
        title: 'Portfolio',
        route: 'portfolio-list',
      },
      {
        title: 'Sell',
        route: 'sell-list',
      },
      {
        title: 'Sell History',
        route: 'sell-history',
      },
    ]
  },
]